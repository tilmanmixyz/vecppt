// SPDX-FileCopyrightText: 2023 Tilman Andre Mix
//
// SPDX-License-Identifier: MIT

#pragma once
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstddef>
#include <optional>
#include <utility>
#ifndef VECPPT_VEC_HPP_
#define VECPPT_VEC_HPP_

template <typename T>
struct Vec {
 private:
  std::size_t _cap = 0;  // Actual Capacity of the vector (cap >= len)
  std::size_t _len = 0;  // How many items are in the vector

  T* _data = nullptr;

  // add_cap: takes thr additional capacity which
  // should be allocated, not the overall capacity
  // meaning: allocated = add_cap + _cap
  void re_alloc(size_t add_cap) {
    std::size_t new_cap = add_cap + _cap;

    T* new_block = new T[new_cap];
    for (std::size_t i = 0; i < _len; i++) {
      new_block[i] = std::move(_data[i]);
    }

    delete[] _data;
    _data = new_block;
    _cap = new_cap;
  }

 public:
  Vec(void) { re_alloc(2); }

  ~Vec(void) { delete[] _data; }

  const std::size_t len(void) const { return this->_len; }

  const std::size_t capacity(void) const { return this->_cap; }

  const bool is_empty(void) const {
    if (this->_len == 0) {
      return true;
    }

    return false;
  }

  void reserve(std::size_t additional) {
    std::size_t empty = this->_cap - this->_len;
    if (empty >= additional) {
      return;
    }

    while (additional < empty) {
      additional *= 2;
    }

    re_alloc(additional);
  }

  void push(const T& value) {
    if (_len >= _cap) {
      // Double the capacity, not change capacity to _cap
      re_alloc(_cap);
    }

    _data[_len++] = std::move(value);
  }

  void push(T&& value) {
    if (_len >= _cap) {
      // Double the capacity, not change capacity to _cap
      re_alloc(_cap);
    }

    _data[_len++] = std::move(value);
  }

  // Unsafe, could index not avalable slot, use get() for optional as return
  const T& operator[](std::size_t index) const { return _data[index]; }

  // Unsafe, could index not avalable slot, use get() for optional as return
  T& operator[](std::size_t index) { return _data[index]; }

  const std::optional<T> get(std::size_t index) const {
    if (index >= _len) {
      return {};
    }

    return _data[index];
  }

  std::int8_t update(std::size_t index, T value) {
    if (_len <= index) {
      return -1;
    }

    _data[index] = value;

    return EXIT_SUCCESS;
  }
};

#endif  // VECPPT_VEC_H_
