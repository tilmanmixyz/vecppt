// SPDX-FileCopyrightText: 2023 Tilman Andre Mix
//
// SPDX-License-Identifier: MIT

#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include "vector.hpp"

template <typename T>
void print_vec(const Vec<T>& vec) {
  for (std::size_t i = 0; i < vec.len(); i++) {
    std::cout << "Index " << i << ": " << vec.get(i).value() << '\n';
  }
}

std::int32_t main(void) {
  Vec<std::string> vector;
  std::string msg = "Hello, World";
  vector.push(msg);
  vector.reserve(4);
  vector.push(msg);
  print_vec(vector);
  std::size_t len = vector.len();
  int return_value = vector.update(len - 1, "Hello");
  print_vec(vector);
  std::printf("%d\n", return_value);

  return EXIT_SUCCESS;
}
