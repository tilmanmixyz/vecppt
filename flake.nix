# SPDX-FileCopyrightText: 2023 Tilman Andre Mix
#
# SPDX-License-Identifier: MIT

{
  description = "Description for the project";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
  };

  outputs = inputs @ {flake-parts, ...}:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
      ];
      systems = ["x86_64-linux" "aarch64-linux" "aarch64-darwin" "x86_64-darwin"];
      perSystem = {
        config,
        self',
        inputs',
        pkgs,
        system,
        ...
      }: let
        buildInputs =
          []
          ++ pkgs.lib.optionals pkgs.stdenv.isDarwin [
            pkgs.libiconv
          ];
        bin = pkgs.clang16Stdenv.mkDerivation {
          inherit buildInputs;
          useMoldLinker = true;
          pname = "vecppt";
          version = "0.1.0";
          src = ./.;

          nativeBuildInputs = [
            pkgs.meson
            pkgs.ninja
            pkgs.pkg-config
          ];

          mesonBuildType = "release";
        };
      in {
        devShells = {
          default = pkgs.mkShell {
            inherit buildInputs;
            inherit (bin) nativeBuildInputs;
            packages = [
              pkgs.reuse
            ];
          };
        };

        packages = {
          inherit bin;
          default = bin;
        };

        formatter = pkgs.alejandra;
      };
      flake = {
      };
    };
}
