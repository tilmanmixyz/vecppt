# vecpp\<T\>

[Rust](https://rust-lang.org) inspired Vectors in C++

## Tutorial

The API is similar to the API of a [Rust](https://rust-lang.org) [`Vec`](https://doc.rust-lang.org/stable/std/vec/struct.Vec.html).

To start using this library move the [include/vector.hpp](include/vector.hpp) header to the location where you store header files.

Using this header requires you to use at at least C++17.

```cpp
#include<vector.hpp>
/*
 * Initialize the vector
 */

Vec<int> vec;

/*
 * Adding elements to the vector
 */
vec.push(1);
vec.push(2); // no allocation as the default size is two(2)
vec.push(3); // First reallocation, adding two(2) extra slots, double of previous
vec.push(4);
vec.push(5); // Third alloration, adding four(4) extra slots, double of previous capacity

/*
 * Getting elements from the vector the unsafe way
 */

int third = vec[2];

/*
 * Getting elements from the vector the safe way, return std::optional<T>, in this case int
 */

int first = vec.get(0).value();

/*
 * Get the capacity of a vector
 */

int cap = vec.capacity();

/*
 * Find out if a vector is empty
 */

bool empty = vec.is_empty();

/*
 * Reserve atleast n slots, might alloc more to not have to allocate often, when called in loop
 */

vec.reserve(12);
```

## Building the test project in [src/main.cpp](src/main.cpp)

### Using [meson](https://mesonbuild.com) and [ninja](https://ninja-build.org)

```sh
meson setup _builddir
ninja -C _builddir
./_builddir/vecppt
```

### Using your local C++ compiler

```sh
# clang
clang++ -Iinclude -o vecppt src/main.cpp
# gcc
g++ -Iinclude -o vecppt src/main.cpp

./vecppt
```

## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## License

Licensed under

 * MIT license
   ([LICENSES/MIT.txt](LICENSES/MIT.txt) or http://opensource.org/licenses/MIT)

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, shall be licensed as MIT, without any additional terms or conditions.
